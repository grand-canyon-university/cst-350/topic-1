## Topic 1: The Agile Method And .NET Foundation

Scrum is the de-facto delivery methodology used today in the industry. Learning an Agile delivery methodology like Scrum will help you manage and plan development tasks within a team. In this topic, you will get an introduction into Scrum, learn about its common ceremonies, and learn how this methodology will be used during the course to help you deliver each of your topic assignments. In this topic, you will also get an introduction to .NET and the ASP.NET (formerly called .NET MVC) framework, which is Microsoft's implementation of one of the most popular design patterns, and learn how the MVC design pattern will be used in your topic assignment.
Objectives:


1. Apply Agile design principles to a software development project.
2. Identify Scrum's common terms, team roles, and ceremonies, and how they are used to help manage the delivery of software projects.
3. Identify Scrum's artifacts such as User Stories, Sprint Plan, Product Back Log, and Sprint Burn Down Chart, and how they are used to help manage the delivery of software projects.
4. Define the core components of .NET framework and how the C# programming language fits into the framework.
5. Define the MVC Design Pattern, identify each of its core components, and how this design pattern solves separation of concerns.
6. Define a Layered .NET Application Architecture, identify each of the Layers, and how this design pattern solves separation of concerns.

### Resources
**ASP.NET Documentation**
[Read the section "ASP.NET Core Overview," from Microsoft (2020).](https://learn.microsoft.com/en-us/aspnet/core/introduction-to-aspnet-core?view=aspnetcore-3.1)

**Intro to Scrum in Under 10 Minutes**
[View "Intro to Scrum in Under 10 Minutes," by Axosoft, from YouTube (2012).](https://www.youtube.com/watch?v=QwQuro7ekvc&t=172s)
